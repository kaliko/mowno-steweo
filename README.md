# MOWNO-STEWEO

A plain shell script to ease listening to mowno [steweo music stream] out of the
browser.

**Print-out a M3U playlist and open it in the default player:**
```sh
./mowno-steweo m3u > steweo.m3u && xdg-open steweo.m3u
```

**Current song info:**
```sh
./mowno-steweo status
```

[steweo music stream]: https://www.mowno.com/steweo/
