#!/bin/sh

STREAM='https://listen.radioking.com/radio/26746/stream/59470'
STATUS='https://www.radioking.com/widgets/api/v1/radio/26746/track/current'

TMP_JSON=$(mktemp --suffix=-steweo.json)

finish () {
  rm -rf "${TMP_JSON}"
}
trap finish EXIT

_is_exe () {
    command -v $1 >/dev/null 2>&1 || return 1
    return 0
}

get_json () {
    if _is_exe curl;then
      curl --silent --fail --show-error --output ${TMP_JSON} ${STATUS} || return $1
    elif _is_exe wget;then
      wget --tries=1 --quiet --output-document=${TMP_JSON} ${STATUS} || \
        { echo "wget returned $?"; return 1; }
    else
      echo 'curl or wget not found!'
      return 1
    fi
}

case $1 in
  mpd)
    _is_exe mpc || { echo "mpc command not found!"; exit 1; }
    mpc add ${STREAM} && mpc --quiet play $(mpc playlist | wc -l)
    ;;
  m3u)
    printf '#EXTM3U\n#EXTINF:-1, Mowno Steweo\n%s\n' ${STREAM}
    ;;
  st*)
    get_json || exit 1
    for val in 'artist' 'album' 'title'; do
      printf '%-7s : ' $val
      grep -oP "(?<=\"$val\":\")[^\"]*" ${TMP_JSON}
    done
    ;;
  *)
    cat <<EOF
$0 [m3u|status|mpd]
  m3u    : print m3u to stdout
  status : print current song info on stdout
  mpd    : add audio stream to MPD and start palying
EOF
    ;;
esac
